# Pers

Pronounce "purse", pers brings you all the command line functionality for your personal accounts that you could want. Instead of having to create a whole web application to do something simple like creating a github repo, pers takes your api keys and ids and provides that functionality for you.

## Setup

First thing you need to do is, in your home directory create `pers.config.json` (or any file specified with PERSCONFIG as an environment variable). Then add the api keys and ids for the functionality you want.

## Github

### Setup

To set up github, you need to [create a personal api token](https://github.com/settings/tokens/new), add that token to the pers.config.json file, like so:

```json
{
  "github": {
    "USERNAME1": "TOKEN1",
    "USERNAME2": "TOKEN2"
  }
}
```


Then you're ready to go.

###  Commands

* Create a new repo with `pers create-repo REPONAME`
* Delete a repo with `pers delete-repo REPONAME`


## Contributing

As you can see, the vision for this project is a bit more grandiose that it's current state, so if you would like to contribute, please, by all means do! Currently, there's just github, but I would be open to facebook, twitter, or whatever, as long as you have a use case! Github alone has a lot more commands that could be added. Those are just the two that really irk me to have to interrupt my coding process to do. 
