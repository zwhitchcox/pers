#!/usr/bin/env node
const minimist = require('minimist')
const mkdirp = require('mkdirp')
const rl = require('readline')
const fetch = require('isomorphic-fetch')
const argv = minimist(process.argv.slice(2), {
  boolean: ['p', 'private', 'help', 'f', 'force']
})
const home = process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME']
const cp = require('child_process')
const CONFIGDIR = process.env.CONFIGDIR || `${home}/.pers.config.json`
const config = require(CONFIGDIR)

const usage = _ => {
  console.log(`You must enter a command like

  pers
    create-repo USERNAME/REPONAME 
      -p,--private
      -d,--description DESCRIPTION
      -h,--homepage HOMEPAGEURL
    delete-repo USERNAME/REPONAME 
      -f,--force (won't prompt for confirmation)

    --help (outputs this screen)
  `.trim())
  process.exit(1)
}

if (argv.help) usage()


const cmd = argv._
if (!cmd.length >= 1) usage()

switch (cmd[0]) {
  case 'create-repo':
    createRepo(argv._.slice(1))
    break
  case 'delete-repo':
    deleteRepo(argv._.slice(1))
    break
  default:
    usage()
    break
}

function deleteRepo(repos) {
  repos.forEach(repo => {
    const [username, reponame] = getrepousername(repo)
    if (!username) console.log('No github username detected') || usage()
    const headers = getHeaders(username)
    const method = 'DELETE'
    confirm('delete this repo', () => {
      fetch(`https://api.github.com/repos/${username}/${reponame}`, { method, headers })
        .then(_=>console.log(`${username}/${reponame} has been deleted`))
        .catch(err=>console.error(`Error deleting ${username}/${reponame}`, err))
    })
  })
}

function createRepo(repos) {
  repos.forEach(repo => {
    const [username, reponame] = getrepousername(repo)
    const headers = getHeaders(username)
    console.log(headers)
    const body = JSON.stringify({
      name: reponame,
      homepage: argv.h || argv.homepage || '',
      description: argv.d || argv.description || '',
      private: argv.p || argv.private || false
    })
    console.log(body)
    const method = 'POST'
    fetch('https://api.github.com/user/repos', { method, body, headers })
      .then(_=>console.log(`${username}/${reponame} has been created.`))
      .catch(err=>console.error(`${username}/${reponame} was not created`, err))
    cp.exec('git remote add origin git@github.com:' + username + '/' + reponame, {
      cwd: process.cwd(),
      stdio: 'inherit',
    })
  })
}


function getrepousername(repo) {
  const arg = repo.split('/')
  let username, reponame
  if (arg.length === 2)
    [username, reponame] = arg
  else if (username = process.env.GITHUBUSERNAME) {
    ;[reponame] = arg
  }
  else if (Object.keys(config.github).length === 1) {
    username = "" + Object.keys(config.github)
    ;[reponame] = arg
  }
  username = username || config.github.default
  if (!username) console.log('No github username detected') || usage()
  return [username, reponame]
}

function getHeaders(username) {
  const token = config.github[username]
  return {
    'Content-Type': 'application/json',
    'Authorization': `token ${config.github[username]}`
  }
}

function confirm(actiondescription, action) {
  const rli = rl.createInterface({
    input: process.stdin,
    output: process.stdout,
  })
  rli.question(`Are you sure you want to ${actiondescription}? (y/n) `, answer => {
    if (answer.toLowerCase() === 'y' || answer.toLowerCase() === 'yes')
      action()
    rli.close()

  })
}
